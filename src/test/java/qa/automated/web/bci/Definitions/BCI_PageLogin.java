package qa.automated.web.bci.Definitions;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import qa.automated.web.bci.Generic.UsoCom;
import qa.automated.web.bci.Launcher.ApplicationLauncher;

public class BCI_PageLogin {

	@When("^ingresa rut \"([^\"]*)\"\\.\\.$")
	public void ingresa_rut(String arg1) throws Throwable {
		ApplicationLauncher.driverWeb.navigate().to(ApplicationLauncher.properties.getBci_url());
		String Saludo = ApplicationLauncher.pageLoginWeb.RetornaSldoBienvenida().trim();
		boolean iguales = Saludo.equalsIgnoreCase(ApplicationLauncher.properties.getSaludoBienvenida().trim());
		assertTrue(iguales);
		ApplicationLauncher.pageLoginWeb.clickCampoRut();
        ApplicationLauncher.pageLoginWeb.ingresarRut(arg1);
		Thread.sleep(2000);
	}

	@Then("^valido rut ingresado con puntos y guiones\\.\\.$")
	public void valido_rut_ingresado_con_puntos_y_guiones() throws Throwable {
		String rut = ApplicationLauncher.pageLoginWeb.obtenerRut();
		boolean resultado = ApplicationLauncher.pageLoginWeb.validarRut(rut);
        assertTrue(resultado);
	}

	@When("^Ingresar letras campo rut \"([^\"]*)\"\\.\\.$")
	public void ingresar_letras_campo_rut(String letra) throws Throwable {
		ApplicationLauncher.pageLoginWeb.ingresarRut(letra);
	}

	@Then("^Comprobar que no ingresa letras\\.\\.$")
	public void comprobar_que_no_ingresa_letras() throws Throwable {
		String rut = ApplicationLauncher.pageLoginWeb.obtenerRut();
		Thread.sleep(2000);
		assertTrue("Rut no permite letras correcto", rut.equalsIgnoreCase("") || UsoCom.isNumeric(rut));
	}

	@When("^Ingresar letra k campo rut \"([^\"]*)\"\\.\\.$")
	public void ingresar_letra_k_campo_rut(String letra) throws Throwable {
		ApplicationLauncher.pageLoginWeb.ingresarRut(letra);
	}

	@Then("^Comprobar que ingresa letra correctamente\\.\\.$")
	public void comprobar_que_ingresa_letra_correctamente() throws Throwable {
		String rut = ApplicationLauncher.pageLoginWeb.obtenerRut();
		Thread.sleep(2000);
		assertTrue("Rut permite letra K correcto", rut.equalsIgnoreCase("k") || rut.equalsIgnoreCase("K"));
	}

	@When("^ingresa rut invalido sin digito verificador\"([^\"]*)\"\\.\\.$")
	public void ingresa_rut_invalido_sin_digito_verificador(String arg1) throws Throwable {
		ApplicationLauncher.pageLoginWeb.ingresarRut(arg1);
		ApplicationLauncher.pageLoginWeb.clickButtoningresar();
	}

	@When("^ingreso en campo clave \"([^\"]*)\" con cinco digitos\\.\\.$")
	public void ingreso_en_campo_clave_con_cinco_digitos(String arg1) throws Throwable {
		ApplicationLauncher.pageLoginWeb.ingresarClave(arg1);
	}

	@Then("^Valido ingreso de clave con largo de clave invalida\\.\\.$")
	public void valido_ingreso_de_clave_con_largo_de_clave_invalida() throws Throwable {
	 
	}

	@When("^ingreso rut \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void ingreso_rut_y_clave(String arg1, String arg2) throws Throwable {
		ApplicationLauncher.pageLoginWeb.ingresarRut(arg1);
		ApplicationLauncher.pageLoginWeb.ingresarClave(arg2);
	}

	@Then("^Se valida ingreso de clave y rut validos de forma exitosa$")
	public void se_valida_ingreso_de_clave_y_rut_validos_de_forma_exitosa() throws Throwable {
		ApplicationLauncher.pageLoginWeb.btnLoginActivo();
		Thread.sleep(5000);
		ApplicationLauncher.pageLoginWeb.ClickBtnVer();
		Thread.sleep(2000);
		ApplicationLauncher.pageLoginWeb.clickButtoningresar();
		Thread.sleep(6000);
	}
}
