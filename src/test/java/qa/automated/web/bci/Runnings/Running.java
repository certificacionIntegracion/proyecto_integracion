package qa.automated.web.bci.Runnings;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

import com.hpe.alm.octane.OctaneCucumber;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.NetworkMode;
import com.cucumber.listener.ExtentCucumberFormatter;

@RunWith(OctaneCucumber.class)
@CucumberOptions(features = "src/test/java/qa/automated/web/bci/Feature", 
glue = "qa/automated/web/bci/Definitions", 
plugin = { "pretty", "html:target/cucumber" , 
		   "json:target/cucumber.json",
		   "com.cucumber.listener.ExtentCucumberFormatter"})

public class Running { 
	public static SimpleDateFormat sdf;
    
    @BeforeClass
    public static void setup() {

		DisplayOrder NEWEST_FIRST = null;
		NetworkMode ONLINE = null;
		String USUARIO = System.getProperty("user.name");
		sdf = new SimpleDateFormat("dd-MM-YYYY_hh-mm-ss");

		ExtentCucumberFormatter.initiateExtentCucumberFormatter(
				new File("C:/Users/" + USUARIO + "/Desktop/Reporte/Reportes " + sdf.format(new Date())
						+ "/Reporte_Automatizacion_" + sdf.format(new Date()) + ".html"),
				false, NEWEST_FIRST, ONLINE, new Locale("en-CL"));

        //Carga la config del xml
        System.out.println("CARGA CONFIGURACIÓN DEL XML");
        ExtentCucumberFormatter.loadConfig(new File("src/test/java/qa/automated/web/bci/Config/extent-config.xml"));

        // Detalle características     
        ExtentCucumberFormatter.addSystemInfo("Nombre Proyecto","BCI Automation");
        ExtentCucumberFormatter.addSystemInfo("Zona Horaria", System.getProperty("user.timezone"));
        ExtentCucumberFormatter.addSystemInfo("Ubicacion Usuario", System.getProperty("user.country"));
        ExtentCucumberFormatter.addSystemInfo("Version OS", System.getProperty("os.version"));
 
    }
}