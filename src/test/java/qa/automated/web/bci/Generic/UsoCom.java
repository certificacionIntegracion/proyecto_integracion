package qa.automated.web.bci.Generic;

import java.io.File;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.JavascriptExecutor;

/**
* CLASE DE USO COMUN CON DIVERSOS METODOS DE USO GENERAL PARA UTILIZAR
*/

public final class UsoCom {

	/**
	 * Metodo que se encarga de esperar X cantidad de segundos durante la ejecucion de la prueba utilizando el driver
	 * 
	 * @param driver
	 * @param segundos
	 */
	public static void esperarSegundos(WebDriver driver, int segundos) {
		synchronized (driver) {
			try {
				driver.wait(segundos * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Fecha actual
	 */
	public static String now() {
		Calendar ahora = Calendar.getInstance();
		Integer n = new Integer((ahora.get(Calendar.YEAR) * 10000) + ((ahora.get(Calendar.MONTH) + 1) * 100) + (ahora.get(Calendar.DAY_OF_MONTH)));
		return n.toString();
	}

	/**
	 * Tomar Screenshoot
	 * 
	 * @param driver
	 * @param nombre
	 *            El cual se dara a la captura
	 */
	public static void screenShot(WebDriver driver, String nombre) {
		try {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir") + "\\Screenshots\\" + nombre + "_" + now() + ".jpg"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cambiar el foco de ventana web
	 * 
	 * @param driver
	 * @param cambiarSoloUna
	 *            En caso de haber mas de dos ventana y solo querer cambiar a la siguiente, true
	 */
	public static void cambiarVentanaWeb(WebDriver driver, boolean cambiarSoloUna) {
		String MainWindow = driver.getWindowHandle();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();

		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
				if (cambiarSoloUna)
					break;
			}
		}
	}

	/**
	 * Cerrar ventana web, solo cierra ventana sin detener driver
	 * 
	 * @param driver
	 *            WebDriver
	 */
	public static void cerrarVentanaWeb(WebDriver driver) {
		String MainWindow = driver.getWindowHandle();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();

		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
				driver.close();
				driver.switchTo().window(ChildWindow);
				
			}
		}
	}

	/**
	 * Sroll hasta final de pagina
	 * 
	 * @param driver
	 *            WebDriver
	 * 
	 * @return void
	 */
	public static void finalVentana(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
	}

	/**
	 * scrollElement, realiza scroll de pagina en donde se encuentra objeto
	 * 
	 * @param driver
	 *            WebDriver
	 * @param element
	 *            WebElement donde se realizara el Scroll
	 * 
	 * @return void
	 */
	public static void scrollElement(WebDriver driver, WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * scrollClickElement, realiza scroll y ademas click al elemento de la pagina
	 * 
	 * @param driver
	 *            WebDriver
	 * @param element
	 *            WebElement donde realizara Scroll y hara click
	 * 
	 * @return void
	 */
	public static void scrollClickElement(WebDriver driver, WebElement element) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cerrar ventana WebDriver
	 * 
	 * @param driver
	 */
	public static void cerrarVentana(WebDriver driver) {
		driver.close();
	}

	/**
	 * Cerrar proceso WebDriver
	 * 
	 * @param driver
	 */
	
	public static void cerrarDriver(WebDriver driver) {
		driver.quit();
	}

	/**
	 * Volver atras
	 * 
	 * @param driver
	 */
	public static void volver(WebDriver driver) {
		driver.navigate().back();
	}

	/**
	 * Comprobar si el valor es numerico
	 * 
	 * @param valor
	 *            Parametro a comprobar si es numerico
	 */
	public static boolean isNumeric(String valor) {
		try {
			Double.parseDouble(valor);
			return true;
		} catch (NumberFormatException ne) {
			return false;
		}
	}

	/**
	 * <b>Nombre:</b> esperaElementoSegundos</br>
	 * </br>
	 * <b>Description:</b> Genera una pausa explicita hasta que el elemento dado es encontrado.
	 * 
	 * @param WebDriver
	 *            Controlador WebDrive.
	 * @param WebElement
	 *            Elemento a esperar.
	 * @param segundos
	 *            (int) Valor de tiempo en segundos a esperar.
	 * @return {@link Boolean} Retorna un valor <b>verdadero</b> si el elemento es encontrado dentro del tiempo estipulado, de lo contrario retorna un valor <b>falso</b>.
	 **/
	public static boolean esperaElementoSegundos(WebDriver driver, WebElement webElement, int segundos) {
		WebDriverWait wait = new WebDriverWait(driver, segundos);
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}
}