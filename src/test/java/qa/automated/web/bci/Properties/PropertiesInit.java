package qa.automated.web.bci.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class PropertiesInit {
	public static Properties parametros;
	
    private String SaludoBienvenida;
    private String bci_url;
    private String browser;
    private String selenium_server_url;
    private String browser_local_driver;
    
    
	public PropertiesInit() {
		Properties parametros;
		File propFileName = new File("src/test/java/qa/automated/web/bci/Config/config.properties").getAbsoluteFile();;
		FileInputStream inputStream;

		
		try {
			inputStream = new FileInputStream(propFileName);
			parametros = new Properties();
			parametros.load(inputStream);

		    bci_url = parametros.getProperty("BCI_URL");
		    browser = parametros.getProperty("BROWSER");
		    selenium_server_url = parametros.getProperty("SELENIUM_SERVER_URL");
		    browser_local_driver = parametros.getProperty("BROWSER_LOCAL_DRIVER");
			SaludoBienvenida = parametros.getProperty("SaludoBienvenida");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Modifica fichero de propiedades
	 * 
	 * @param fichero
	 *            Archivo properties a modificar
	 * @param key
	 *            Nombre de properties a modificar
	 * @param value
	 *            Valor que se modificara en propertie
	 * 
	 * @return void
	 */

	public String getSaludoBienvenida() {
		return SaludoBienvenida;
	}

	public void setSaludoBienvenida(String saludoBienvenida) {
		SaludoBienvenida = saludoBienvenida;
	}

	public static Properties getParametros() {
		return parametros;
	}

	public static void setParametros(Properties parametros) {
		PropertiesInit.parametros = parametros;
	}

	public String getBci_url() {
		return bci_url;
	}

	public void setBci_url(String bci_url) {
		this.bci_url = bci_url;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getSelenium_server_url() {
		return selenium_server_url;
	}

	public void setSelenium_server_url(String selenium_server_url) {
		this.selenium_server_url = selenium_server_url;
	}

	public String getBrowser_local_driver() {
		return browser_local_driver;
	}

	public void setBrowser_local_driver(String browser_local_driver) {
		this.browser_local_driver = browser_local_driver;
	}
}
